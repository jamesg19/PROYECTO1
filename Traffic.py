import copy
import asyncio
import threading
import random
import time
from Individual import Individual


class Traffic:
    def __init__(self,population,num_generations_max,type_completion, each_generations,do_mutations, value_criteria,nodes,edges,table,completed_departures_label, num_generations_label,efficiency_label,canvas):

        self.num_population=population
        self.num_generations_max=num_generations_max
        self.type_completion=type_completion
        self.each_generations=each_generations
        self.do_mutations=do_mutations
        self.value_criteria=value_criteria
        self.table = table
        self.nodes=[node.copy_node(node) for node in nodes]
        self.edges= [edge.copy_edge(edge) for edge in edges]
        self.population=[]
        self.best_individual=None
        self.completed_departures_label = completed_departures_label
        self.num_generations_label = num_generations_label
        self.efficiency_label = efficiency_label
        self.canvas=canvas
        self.count_generations=0

    def run(self):
        self.table.clear_table()
        self.generate_individual()
        self.generations()
        #print("------------NODO ORIGINAL -------------------")
        #self.population[0].info_edges()
        #print("\n\n")
        #self.mutation_gen(self.population[0])
        #Generar Individuos
        #for i in range(len(self.poblacion)):



    def generate_individual(self):

        for i in range(self.num_population):
            individuals = Individual(self.nodes, self.edges)
            individuals.random()
            individuals.get_aptitude()
            self.population.append(individuals)
    def generations(self):
        if self.type_completion == "Generation":
            self.num_generations_max = self.value_criteria

        for generacion in range(self.num_generations_max):
            self.count_generations += 1

            #time_sleep = 1/1000
            #time.sleep(time_sleep)

            parentA, parentB = self.roulette_method()
            childA, childB = self.crossing(parentA,parentB)
            # Ordenamos de mayor a menor
            self.population = sorted(self.population, key=lambda x: x.aptitude, reverse=True)
            # Sustituimos los individuos con menos aptitud y los sustituimos por los hijos A y B
            self.population[-1] = childA
            self.population[-2] = childB

            # Imprimir el individuo con mas aptitud de la lista hasta el momento
            #print(f"@@@@@@@@@@@@@@@@@ APTITUD{self.population[0].aptitude}")
            #print(f"@@@@@@@@@@@@@@@@@SALIDAS{self.population[0].num_car_exits}")
            #print(f"@@@@@@@@@@@@@@@@@ENTRADAS{self.population[0].num_entry_cars}")
            #self.population[0].info_edges()
            child=[]
            child.append(childA)
            child.append(childB)
            if (self.count_generations % self.each_generations == 0) and self.do_mutations >0:
                # Llamar al método mutacion n veces
                for _ in range(self.do_mutations):
                    self.mutation_gen(child)

            #print("\n")
            self.best_individual = self.population[0]
            self.show_percentage()
            #Completion criteria
            if (self.type_completion == "Efficiency" and self.population[0].aptitude >= self.value_criteria):
                break
            #self.table.clear_table()
            #self.table.add_data(self.population[0].edge)
        self.table.clear_table()
        self.table.add_data(self.population[0].edge)


    def show_percentage(self):
        #self.canvas.itemconfig(self.completed_departures_label, text=f"{self.best_individual.num_car_exits} ")
        self.completed_departures_label.config(text=f"Salidas completadas: {self.best_individual.num_car_exits}")

        #self.canvas.itemconfig(self.num_generations_label, text=f"{self.count_generations} ")
        self.num_generations_label.config(text=f"Generaciones: {self.count_generations} ")

        #self.canvas.itemconfig(self.efficiency_label,text=f"{(self.best_individual.num_car_exits / self.best_individual.num_entry_cars) * 100} %")
        self.efficiency_label.config(text=f"Eficiencia: {(self.best_individual.num_car_exits / self.best_individual.num_entry_cars) * 100} %")
        for item in self.edges:
            for best in self.best_individual.edge:
                if item.name == best.name:
                    self.canvas.itemconfig(item.text_item, text=f"{best.porcentaje} %")




    def roulette_method(self):
        #Hacer un random entre los individuos generados
        random.shuffle(self.population)
        total=0
        for individuals in self.population:
            total += individuals.aptitude
        parents=[]
        #print("Total: ", total)
        #select two
        for _ in range(2):
            random_num = random.randint(1, total)
            temp_aptitud = 0
            for individual in self.population:

                temp_aptitud += individual.aptitude

                if temp_aptitud >= random_num:
                    parents.append(individual)
                    break
        return parents

    def crossing(self,individuoA,individuoB):
        #print("/*/*/*/*/*/*/*/*/* Parent A /*/*/*/*/*/*/*/*/*")
        #individuoA.info_edges()
        #print("/*/*/*/*/*/*/*/*/* Parent B /*/*/*/*/*/*/*/*/*")
        #individuoB.info_edges()
        #print("\n\n\n")
        num_gen1= random.randint(1,  (len(individuoA.node))-1)
        num_gen2 = random.randint(1, (len(individuoA.node))-1)
        #numero_aleatorio = random.randint(0,  len(individuoA.node))
        #print("Numero de genes: ",num_gen1, " TOTAL: ", len(individuoA.node))

        genA1,genA2 = individuoB.node[:num_gen1], individuoA.node[num_gen1:] #list of nodos for child A
        genB1,genB2 = individuoA.node[:num_gen2], individuoB.node[num_gen2:]

        childA = self.mix_gen(genA1,genA2,individuoA.edge, individuoB.edge)
        childB = self.mix_gen(genB1, genB2, individuoA.edge, individuoB.edge)
        return childA, childB

    def mutation_gen(self,child):
        individuoA=random.choice(child)
        #pos_gen_mutate = random.randint(0, (len(individuoA.node))-1)
        pos_gen_mutate = 3
        #print("Posicion de mutacion: ",pos_gen_mutate)
        #print("Nodo a mutar: ",individuoA.node[pos_gen_mutate].name)
        node_mutate= individuoA.node[pos_gen_mutate].copy_node(individuoA.node[pos_gen_mutate])
        node_mutate.reset_value()
        individuoA.mutation(node_mutate)
        edgeTemp = []
        for g in individuoA.node:#node
            #g.print_info()
            g.reset_value()

            for i in individuoA.edge:
                if g.name == i.start_node.name:
                    edgeTemp.append(i)
                    i.reset_value()
        individuoA.calculate_with_new_genes()
        #individuoA.info_edges()

    def mix_gen(self,gen1,gen2, edge1,edge2):
        gen1 = [node.copy_node(node) for node in gen1]
        gen2 = [node.copy_node(node) for node in gen2]
        edge1 = [edge.copy_edge(edge) for edge in edge1]
        edge2 = [edge.copy_edge(edge) for edge in edge2]
        edgeTemp=[]
        nodeTemp=[]
        #print("Gen1 Padre A")
        for g in gen1:#node
            #g.print_info()
            g.reset_value()

            for i in edge1:
                if g.name == i.start_node.name:
                    edgeTemp.append(i)
                    i.reset_value()

        #print("Gen1 Padre B")
        for g in gen2:#node
            #g.print_info()
            g.reset_value()

            for i in edge2:
                if g.name == i.start_node.name:
                    edgeTemp.append(i)
                    i.reset_value()

        nodeTemp = gen1+gen2

        child = Individual(nodeTemp,edgeTemp)
        child.calculate_with_new_genes()
        child.get_aptitude()
        #print("/*/*/*/*/*/*/*/*/* CHILD /*/*/*/*/*/*/*/*/*")
        #print("/*/*/*/*/*/*/*/*/* CHILD /*/*/*/*/*/*/*/*/*")
        #child.info_edges()
        #print("\n\n")
        return child

    def info_general(self):
        #print("/*/*/*/*/*/*/*/*/* INFO GENERAL /*/*/*/*/*/*/*/*/*")
        #print("/*/*/*/*/*/*/*/*/* INFO GENERAL /*/*/*/*/*/*/*/*/*")
        count=0
        for item in self.population:
            count+=1
            print(f"/*/*/*/*/*/*/*/*/* ITERACION {count} /*/*/*/*/*/*/*/*/*")
            item.info_edges()
        print("\n\n")

