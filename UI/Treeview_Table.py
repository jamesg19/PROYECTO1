class Treeview_Table:
    def __init__(self,tk, ttk,tool_panel):
        self.tk=tk
        self.ttk =ttk
        self.tool_panel=tool_panel
        # Crear la tabla debajo de las propiedades
        # Crear la tabla debajo de las propiedades
        # Crear la tabla debajo de las propiedades
        self.table = ttk.Treeview(self.tool_panel,
                                  columns=("Tipo","Desde", "Hasta", "Capacidad", "Entrada_Carro", "%", "Salida"),
                                  show="headings")
        self.table.pack()

        # Encabezados de la tabla
        headers = ["Tipo","Desde", "Hasta", "Capacidad", "Entrada_Carro", "%", "Salida"]
        for idx, header in enumerate(headers):
            # Define un ancho mínimo para cada columna de encabezado
            self.table.column(header, width=55, minwidth=55, anchor=tk.CENTER)
            self.table.heading(header, text=header)


    def add_data(self,edges_data):
        # Insertar datos en la tabla
        for i, edge in enumerate(edges_data):
            if edge.end_node != None:
                self.table.insert("", "end", text=str(i),
                            values=(edge.type_edge,edge.start_node.name, edge.end_node.name, edge.capacity, edge.entrada, edge.porcentaje, edge.salida))
            else:
                self.table.insert("", "end", text=str(i),
                                  values=(edge.type_edge,edge.start_node.name, '', edge.capacity, edge.entrada,
                                          edge.porcentaje, edge.salida))

    # Función para limpiar la tabla eliminando todos los elementos excepto los encabezados
    def clear_table(self):
        for item in self.table.get_children():
            self.table.delete(item)