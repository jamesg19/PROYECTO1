import copy
class Node:
    def __init__(self,canvas, x, y, name, node_type, color):
        self.x = x
        self.y = y
        self.name = name
        self.node_type = node_type
        self.color = color
        self.radio = 40
        self.item = None
        self.text_item = None
        self.entrada = 0
        self.salida = 0


    def create_node(self,canvas):
        x0 = self.x - self.radio
        y0 = self.y - self.radio
        x1 = self.x + self.radio
        y1 = self.y + self.radio

        self.item = canvas.create_oval(x0, y0, x1, y1, fill=self.color, tags=("node",))
        self.text_item = canvas.create_text(self.x, self.y - self.radio - 15, text=self.name, tags=("node",),font=("Helvetica", 12))  # Cambia 12 al tamaño de fuente deseado

    def move(self,delta_x, delta_y,canvas):
        self.x += delta_x
        self.y += delta_y
        canvas.move(self.item, delta_x, delta_y)
        canvas.move(self.text_item, delta_x, delta_y)

    # Eliminar el nodo del lienzo y el texto
    def remove(self,node_id,canvas):
        canvas.delete(self.text_item)
        canvas.delete(node_id)

    def clean(self):
        self.item = None


    def print_info(self):
        print("Nombre: ",self.name," entrada: ",self.entrada," salida: ",self.salida)

    def copy_node(self, node):

        new_node = Node(None, node.x, node.y, node.name, node.node_type, node.color)
        new_node.radio = node.radio
        new_node.item = None
        new_node.text_item = None
        new_node.entrada = node.entrada
        new_node.salida = node.salida
        return new_node

    def calc_num_cars_in_entrance(self, edges):
        for edge in edges:

            if edge.type_edge == "entrance" and edge.start_node.name == self.name:
                self.entrada += edge.entrada
                #print("James: ",self.entrada)

    def calc_num_cars_in_node(self, edges):
        for edge in edges:

            if edge.type_edge == "one_way" and edge.end_node.name == self.name:
                #print("Nodo: ",self.name," Desde: ", edge.start_node.name," Hasta: ", edge.end_node.name, " Set+= ",edge.end_node.entrada)
                self.entrada += edge.end_node.entrada
    def calc_num_car_in_node_exit(self, edges,edgeS):

        self.entrada=0
        for edge in edges:
            if edge.end_node != None and edge.end_node.name == self.name and edge.type_edge == "one_way":
                self.entrada += edge.salida


    def reset_value(self):

        self.entrada = 0
        self.salida = 0




