import asyncio
import pickle
import threading
import tkinter as tk
from tkinter import ttk, messagebox, filedialog

from Traffic import Traffic
from UI.Edge import Edge
from File.InstanceSaver import InstanceSaver
from Logica.Model import Model
from UI.ModelVisualizer import ModelVisualizer
from UI.Node import Node
from UI.Treeview_Table import Treeview_Table


class StreetSystemGUI:
    def __init__(self, root):
        self.root = root
        self.root.title("Street System Modeler")
        width = root.winfo_screenwidth()*0.7
        height = root.winfo_screenheight()*0.6
        # Crear el lienzo para dibujar el grafo width=2560, height=1980
        self.canvas = tk.Canvas(root, width=width, height=height, bg="white")
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        # Panel de herramientas
        self.tool_panel = tk.Frame(root)
        self.tool_panel.pack(side=tk.RIGHT, fill=tk.Y)
        # Define el estilo con el tamaño de la fuente deseado
        style = ttk.Style()
        style.configure("TButton", font=("Arial", 12))
        pady=15
        padx=30
        self.add_node_button = ttk.Button(self.tool_panel, text="Agregar Nodo", command=self.add_node)
        self.add_node_button.pack(pady=pady, padx=padx, ipadx=padx, ipady=pady)  # Ajustar el tamaño del botón

        # command=self.show_edge_menu, self.start_adding_edge
        self.add_edge_button = ttk.Button(self.tool_panel, text="Agregar Arista", command=self.show_edge_menu)
        self.add_edge_button.pack(pady=pady, padx=padx, ipadx=padx, ipady=pady)
        self.remove_button = ttk.Button(self.tool_panel, text="Eliminar Elemento", command=self.remove_element)
        self.remove_button.pack(pady=pady, padx=padx, ipadx=padx, ipady=pady)

        # Propiedades del elemento seleccionado
        # Crear el primer label
        self.selected_item_label = ttk.Label(self.tool_panel, text="INFORMACION:")
        self.selected_item_label.pack(side="top")

        # Crear el segundo label
        self.completed_departures_label = ttk.Label(self.tool_panel, text="Salidas completadas: ")
        self.completed_departures_label.pack(side="top")
        self.num_generations_label = ttk.Label(self.tool_panel, text="Generaciones: ")
        self.num_generations_label.pack(side="top")
        self.efficiency_label = ttk.Label(self.tool_panel, text="Eficiencia: ")
        self.efficiency_label.pack(side="top")

        # Lista de nodos y aristas
        self.nodes = []
        self.edges = []
        self.type_edge=None
        # Contador para asignar nombres a los nodos
        self.node_counter = 0
        # Contador para asignar nombres a los nodos
        self.edge_counter = 0
        # Variables para almacenar la posición del nodo en el momento del clic
        self.drag_data = {"x": 0, "y": 0, "item": None}
        # Variables para almacenar los nodos seleccionados para la creación de aristas
        self.selected_node1 = None
        self.selected_node2 = None
        # Indicador de si se está agregando una arista
        self.adding_edge = False
        # Menú para seleccionar el tipo de nodo
        self.node_menu = None
        # Asociar eventos de arrastrar y soltar
        self.canvas.tag_bind("node", "<ButtonPress-1>", self.on_drag_start)
        self.canvas.tag_bind("node", "<ButtonRelease-1>", self.on_drag_stop)
        self.canvas.tag_bind("node", "<B1-Motion>", self.on_dragging)
        self.canvas.bind("<Button-1>", self.on_click)
        # Crear el menú desplegable
        self.menu_bar = tk.Menu(root)
        self.root.config(menu=self.menu_bar)

        # Menú Archivo
        self.file_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="Archivo", menu=self.file_menu)
        #self.file_menu.add_command(label="Guardar", command=self.save_model)
        self.file_menu.add_command(label="Guardar Modelo", command=self.save_model)
        self.file_menu.add_command(label="Cargar Modelo", command=self.load_model)
        self.file_menu.add_separator()
        self.file_menu.add_command(label="Salir", command=root.quit)
        self.run_option = tk.Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="Ejecutar", menu=self.run_option)
        #self.run_option.add_command(label="Run", command=self.simulate)
        self.run_option.add_command(label="Run", command=self.show_window)
        self.table=Treeview_Table(tk,ttk,self.tool_panel)
        self.data_window=None
        self.size_population = None
        self.each_generations = None
        self.do_mutations = None
        self.type_completion_combobox = None
        self.value_criteria = None
        self.file_loaded=False
        self.path_file=None

    #node_type, color
    def add_node(self, node_type="simple", color="blue"):
        # Incrementar el contador de nodos
        self.node_counter += 1

        # Crear un nuevo nodo en el lienzo
        x, y = 100, 100  # Coordenadas del nuevo nodo (puedes cambiar estas coordenadas)
        node_name = f"Node{self.node_counter}"
        node = Node(self.canvas, x, y, node_name, node_type, color)
        node.create_node(self.canvas)

        # Guardar el nodo en la lista de nodos
        self.nodes.append(node)

        # Ocultar el menú después de seleccionar un tipo de nodo
        if self.node_menu:
            self.node_menu.destroy()

    def show_edge_menu(self):
        # Crear el menú desplegable para seleccionar el tipo de arista
        self.edge_menu = tk.Menu(self.root, tearoff=0)
        self.edge_menu.configure(font=("Arial", 14))
        self.edge_menu.add_command(label="One way", command=lambda: self.start_adding_edge("one_way"))
        self.edge_menu.add_command(label="Entrada", command=lambda: self.start_adding_edge("entrance"))
        self.edge_menu.add_command(label="Salida", command=lambda: self.start_adding_edge("exit"))
        # Mostrar el menú en la posición del botón "Agregar Arista"
        self.edge_menu.post(self.add_edge_button.winfo_rootx(),
                            self.add_edge_button.winfo_rooty() + self.add_edge_button.winfo_height())

    def start_adding_edge(self,type_edge):

        # Activar el modo de agregar arista
        self.type_edge=type_edge
        self.adding_edge = True
        self.selected_node1 = None
        self.selected_node2 = None
        self.add_edge_button.config(state=tk.DISABLED)  # Deshabilitar el botón "Agregar Arista"
        print("Seleccione el nodo de origen")

    def add_edge(self):
        self.edge_counter+= 1
        if self.type_edge =="one_way" or self.type_edge =="two_way":
            # Verificar si se han seleccionado nodos para crear una arista
            if self.selected_node1 and self.selected_node2:

                if self.selected_node1 != self.selected_node2:

                    edge = Edge(self.edge_counter, self.selected_node1, self.selected_node2,self.type_edge,0,0)
                    edge.create_edge(self.canvas,tk)
                    self.edges.append(edge)
                    self.selected_node1 = None
                    self.selected_node2 = None
                    self.add_edge_button.config(state=tk.NORMAL)  # Habilitar el botón "Agregar Arista"
                    self.adding_edge = False
                    print("Arista agregada")
                else:
                    print("No puedes agregar una arista al mismo nodo")
                    self.selected_node1 = None
                    self.selected_node2 = None
                    self.add_edge_button.config(state=tk.NORMAL)  # Habilitar el botón "Agregar Arista"
                    self.adding_edge = False
        else:

            # Verificar si se han seleccionado nodos para crear una arista
            if self.selected_node1:

                edge = Edge(self.edge_counter,self.selected_node1, None, self.type_edge,0,0)
                edge.create_edge(self.canvas, tk)

                self.edges.append(edge)
                self.selected_node1 = None
                self.selected_node2 = None
                self.add_edge_button.config(state=tk.NORMAL)  # Habilitar el botón "Agregar Arista"
                self.adding_edge = False
                print("Arista agregada-> ",self.type_edge)


    def remove_element(self):
        # Desactivar el modo de agregar arista
        self.adding_edge = False
        self.remove_button.config(state=tk.DISABLED)  # Habilitar el botón "REMOVER ELEMENTO"

        # Solicitar al usuario seleccionar un nodo para eliminar
        print("Seleccione un elemento ")

        # Asignar un evento de clic al lienzo para seleccionar un nodo
        self.canvas.bind("<Button-1>", self.select_node_to_remove)

    def select_node_to_remove(self, event):
        # Identificar el elemento seleccionado en el lienzo
        item = self.canvas.find_closest(event.x, event.y)
        # Verificar si el elemento seleccionado es un nodo
        if "node" in self.canvas.gettags(item):
            # Eliminar el nodo y todas sus aristas
            node_id = item[0]
            self.remove_node(node_id)


        # Verificar si el elemento seleccionado es un nodo
        if "edge" in self.canvas.gettags(item):
            # Eliminar la arista seleccionada
            edge_id = item[0]
            self.remove_edge(edge_id)


        # Detener el evento de clic en el lienzo
        self.canvas.unbind("<Button-1>")
        self.canvas.bind("<Button-1>", self.on_click)#Retorno a la funcionalidad de onclik

    def remove_node(self, node_id):
        # Eliminar el nodo y todas sus aristas
        for node in self.nodes:
            if node.item == node_id:
                # Eliminar todas las aristas conectadas al nodo
                tmpEdge=[]
                for edge in self.edges:
                    if node.name == edge.start_node.name:
                        edge.remove(self.canvas)
                        #self.edges.remove(edge)
                        tmpEdge.append(edge)

                    if (edge.end_node != None and node.name == edge.end_node.name):
                        edge.remove(self.canvas)
                        tmpEdge.append(edge)
                        #self.edges.remove(edge)
                for edge in tmpEdge:
                    for edgeG in self.edges:
                        if edge.id == edgeG.id:
                            self.edges.remove(edgeG)

                # Eliminar el nodo del lienzo y el texto
                node.remove(node_id,self.canvas)
                # Eliminar el nodo de la lista de nodos arreglo
                self.nodes.remove(node)
                self.remove_button.config(state=tk.NORMAL)  # Habilitar el botón "REMOVER ELEMENTO"
                print("Nodo eliminado junto con todas sus aristas")
                break
        self.remove_button.config(state=tk.NORMAL)  # Habilitar el botón "REMOVER ELEMENTO"

    def remove_edge(self, edge_id):
        # Eliminar la arista de la lista de aristas
        for edge in self.edges:
            if edge.line == edge_id:
                edge.remove_edge_only(self.canvas)
                self.edges.remove(edge)  # Eliminar la arista de la lista de aristas
                self.remove_button.config(state=tk.NORMAL)  # Habilitar el botón "REMOVER ELEMENTO"
                print("Arista eliminada")
                break
        self.remove_button.config(state=tk.NORMAL)  # Habilitar el botón "REMOVER ELEMENTO"

    def on_drag_start(self, event):
        # Almacenar la posición del nodo en el momento del clic
        self.drag_data["item"] = self.canvas.find_closest(event.x, event.y)[0]
        self.drag_data["x"] = event.x
        self.drag_data["y"] = event.y

    def on_dragging(self, event):
        # Calcular el desplazamiento del nodo
        delta_x = event.x - self.drag_data["x"]
        delta_y = event.y - self.drag_data["y"]

        # Mover el nodo y sus aristas conectadas
        for node in self.nodes:
            if node.item == self.drag_data["item"]:
                node.move(delta_x, delta_y,self.canvas)
                for edg in self.edges:

                    edg.update_edge(self.canvas)
                break

        # Actualizar la posición del nodo en el diccionario drag_data
        self.drag_data["x"] = event.x
        self.drag_data["y"] = event.y

    def on_drag_stop(self, event):
        # Reiniciar los datos de arrastrar y soltar
        self.drag_data["item"] = None
        self.drag_data["x"] = 0
        self.drag_data["y"] = 0

    def on_click(self, event):
        # Manejar el evento de clic del mouse para seleccionar nodos para la creación de aristas
        if self.adding_edge:

            item = self.canvas.find_closest(event.x, event.y)
            tags = self.canvas.gettags(item)

            if "node" in tags:
                if not self.selected_node1:

                    for node in self.nodes:
                        if node.item == item[0]:
                            self.selected_node1 = node

                            if self.type_edge == "entrance" or self.type_edge=="exit":
                                print("es entrada o salida")
                                self.add_edge()
                                break
                            print("Seleccione el nodo de destino")
                            break
                elif not self.selected_node2:

                    for node in self.nodes:
                        if node.item == item[0]:
                            self.selected_node2 = node
                            self.add_edge()
                            break

        else:
            item = self.canvas.find_closest(event.x, event.y)
            tags = self.canvas.gettags(item)
            if "node" in tags:
                # Hacer algo con el nodo seleccionado
                pass

    def save_model(self):
        # Guardar el modelo de nodos y aristas
        nodes_to_save = self.nodes
        edges_to_save = self.edges
        model = Model(nodes_to_save, edges_to_save, self.node_counter, self.edge_counter)
        model.prepare_object_to_save()
        # Crear el InstanceSaver para guardar y cargar modelos
        model_saver = InstanceSaver("")
        if self.path_file == None:

            self.path_file=model_saver.save_instance(model)

        else:
            model_saver.update_instance(self.path_file,model)



    def load_model(self):
        self.canvas.delete("all")
        self.nodes = []
        self.edges = []
        self.path_file=None
        model_saver = InstanceSaver("./Data/modelo3.pkl")
        # Cargar el modelo de nodos y aristas
        loaded_model, self.path_file = model_saver.load_instance()

        if loaded_model:
            self.nodes, self.edges=loaded_model.nodes1, loaded_model.edges1

            visualizer=ModelVisualizer(self.canvas,tk,loaded_model)

            visualizer.add_edges_loaded()
            visualizer.add_nodes_loaded()
            self.node_counter = loaded_model.node_counter
            self.edge_counter = loaded_model.edge_counter

    def simulate(self):

        print(self.size_population.get())
        print(self.each_generations.get())
        print(self.do_mutations.get())
        print(self.type_completion_combobox.get())
        print(self.value_criteria.get())
        self.print_info_general()
        if len(self.nodes) ==0:
            messagebox.showinfo("Alert", "Draw a model.")
            return
        # poblacion,generaciones,mutaciones,nodes,edges
        traffic = Traffic(int(self.size_population.get()), 1000,self.type_completion_combobox.get(), int(self.each_generations.get()), int(self.do_mutations.get()),int(self.value_criteria.get()), self.nodes, self.edges,self.table,self.completed_departures_label,self.num_generations_label,self.efficiency_label, self.canvas)
        # Crear e iniciar un hilo para ejecutar el método run()
        self.thread = threading.Thread(target=traffic.run)
        self.thread.start()
        self.data_window.destroy()
        #best_individual=traffic.best_individual
        #self.show_percentage(best_individual)

    def show_window(self):
        self.data_window = tk.Toplevel()
        self.data_window.title("Ingresar Datos")

        tk.Label(self.data_window, text="Size population:").grid(row=1, column=0,sticky="w")
        self.size_population = tk.Entry(self.data_window)
        self.size_population.grid(row=1, column=1)
        self.size_population.insert(0, 100)

        #Mutations
        tk.Label(self.data_window, text="Mutations:").grid(row=3, column=0, columnspan=2,sticky="w")
        tk.Label(self.data_window, text="For each generations:").grid(row=4, column=0)
        self.each_generations = tk.Entry(self.data_window)
        self.each_generations.grid(row=4, column=1)
        self.each_generations.insert(0, 30)
        tk.Label(self.data_window, text="Do mutations:").grid(row=4, column=2)
        self.do_mutations = tk.Entry(self.data_window)
        self.do_mutations.grid(row=4, column=3)
        self.do_mutations.insert(0, 5)

        # Competion criteria
        tk.Label(self.data_window, text="Completion Criteria:").grid(row=5, column=0, columnspan=2, sticky="w")
        tk.Label(self.data_window, text="Select a type:").grid(row=6, column=0, sticky="w")
        self.type_completion_combobox = ttk.Combobox(self.data_window, values=["Generation", "Efficiency"])
        self.type_completion_combobox.grid(row=6, column=1, sticky="w")
        self.type_completion_combobox.set("Efficiency")
        self.value_criteria = tk.Entry(self.data_window)
        self.value_criteria.grid(row=6, column=2)
        self.value_criteria.insert(0, 90)


        # Configurando validación de entrada a numeros
        validate_cmd = self.data_window.register(self.validate_number)
        self.do_mutations.config(validate="key", validatecommand=(validate_cmd, "%P"))
        self.each_generations.config(validate="key", validatecommand=(validate_cmd, "%P"))
        self.value_criteria.config(validate="key", validatecommand=(validate_cmd, "%P"))
        self.size_population.config(validate="key", validatecommand=(validate_cmd, "%P"))

        boton_enviar = tk.Button(self.data_window, text="Run...", command=self.simulate)
        boton_enviar.grid(row=7, column=0, columnspan=2)
    def validate_number(self,text):
        if text.isdigit():
            if int(text) >= 0:
                return True
        if text =="":
            text=0
            return True
        return False


    def show_percentage(self,best_individual):
        for item in self.edges:
            for best in best_individual.edge:
                if item.name == best.name:
                    # Suponiendo que 'canvas' es tu lienzo y 'self.text_item' es el identificador del objeto de texto
                    self.canvas.itemconfig(item.text_item, text=f"{best.porcentaje} %")


    def print_info_general(self):

        print("\n_______Edges______")
        for item in self.edges:
            item.set_value()
            item.print_info()


    def print_data(self):
        print(self.size_population.get())
        print(self.each_generations.get())
        print(self.do_mutations.get())
        print(self.type_completion_combobox.get())
        print(self.value_criteria.get())