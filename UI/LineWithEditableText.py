import math
import copy

class LineWithEditableText:
    def __init__(self,tk, canvas, x,y,capacity,text):

        self.x=x
        self.y=y
        self.deltaEnt = 30
        self.capacity_field=None
        self.capacity=capacity
        # Crear la etiqueta de texto
        self.text_label = tk.Label(canvas, text=text+":", font=("Arial", 12))
        self.text_label_window = canvas.create_window(self.x - self.deltaEnt-5, self.y, window=self.text_label, anchor='e', width=35)
        # Crear el campo de texto editable
        self.text_entry = tk.Entry(canvas, font=("Arial", 12))
        self.text_entry.insert(0, self.capacity)  # Insertar el nombre inicial
        self.capacity_field = canvas.create_window(self.x - self.deltaEnt, self.y, window=self.text_entry, anchor='w',width=45, tags=("edge",))
        # Vincular el evento de entrada de texto a una función
        self.text_entry.bind("<KeyRelease>", lambda event: self.on_text_input(tk, event))

    def on_text_input(self,tk, event):
        # Esta función se llama cada vez que se ingresa un valor en el campo de entrada
        new_text = self.text_entry.get()
        # Verificar si el nuevo texto es un número o si está vacío
        if new_text.isdigit() or new_text == "":
            # Si es un número o está vacío, actualizar el valor de capacidad
            self.capacity = new_text
        else:
            # Si no es un número, eliminar el último carácter ingresado
            self.text_entry.delete(len(new_text) - 1, tk.END)
    def get_text(self):

        return int(self.text_entry.get())


    def update_text(self,canvas,x,y):
        #canvas.coords(self.text_item, x, y)
        if self.text_label != None:
            self.text_label.bind("<B1-Motion>")
        else:
            print("Entra")
        canvas.coords(self.capacity_field, x- self.deltaEnt, y)
        canvas.coords(self.text_label_window, x - self.deltaEnt-5, y)
        #canvas.coords(self.text_label, x - self.deltaEnt - 5, y)

    def delete_text(self,canvas):
        canvas.delete(self.capacity_field)
        canvas.delete(self.text_label_window)


    def delete_entry(self,):
        self.text_entry=None
        self.text_label=None
        self.text_label_window=None


