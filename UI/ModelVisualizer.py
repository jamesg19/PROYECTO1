
class ModelVisualizer:
    def __init__(self,canvas,tk, model):
        self.canvas=canvas
        self.tk = tk
        self.model = model


    def add_nodes_loaded(self):

        for nodes in self.model.nodes1:
            nodes.create_node(self.canvas)

    def add_edges_loaded(self):

        for edges in self.model.edges1:
            edges.create_edge(self.canvas,self.tk)

