import math
from math import degrees, atan2
import copy
from UI.LineWithEditableText import LineWithEditableText

class Edge:
    def __init__(self,id, start_node, end_node,type_edge,capacity,minimum):
        self.id=id
        self.name=type_edge+f"{self.id}".strip()
        self.start_node = start_node
        self.end_node = end_node
        self.type_edge=type_edge
        self.line = None
        self.text_item = None
        self.capacity_field = None
        self.minimum_field = None
        self.minimum = minimum
        self.capacity = capacity
        self.porcentaje=0
        self.entrada=0
        self.salida=0

    def create_edge(self, canvas,tk):
        x1, y1 = self.start_node.x, self.start_node.y
        deltaEnt=135
        deltaEx=40

        if self.type_edge =="entrance":
            self.line = canvas.create_line(x1-deltaEnt, y1, x1-deltaEx, y1, fill="green", width=9,  tags=("edge",))
            self.capacity_field = LineWithEditableText(tk,canvas, self.start_node.x - deltaEnt + 60, y1 - 24,self.capacity,"Cap")
            self.minimum=100
            #self.minimum_field = LineWithEditableText(tk, canvas, self.start_node.x - deltaEnt + 60, y1 - 48, self.capacity, "Min")


        elif self.type_edge =="exit":
            self.line = canvas.create_line(x1+deltaEx, y1, x1+deltaEnt, y1, fill="red", width=9,  tags=("edge",))
            self.capacity_field = LineWithEditableText(tk, canvas, self.start_node.x+ deltaEnt + 60, y1 - 24 , self.capacity,"Cap")
            self.text_item = canvas.create_text( self.start_node.x+ deltaEnt + 95, y1 - 24, text=self.porcentaje, tags=("node",),font=("Helvetica", 12))  # Cambia 12 al tamaño de fuente deseado
            self.minimum = 100
            #self.minimum_field = LineWithEditableText(tk, canvas, self.start_node.x + deltaEnt + 60, y1 - 48, self.capacity, "Min")

        else:
            x2, y2 = self.end_node.x, self.end_node.y
            x_inicio,y_inicio,x_fin,y_fin,distancia=self.calc_coord(x1,y1)

            if self.type_edge =="one_way":

                text_x, text_y, angle = self.calc_angle(x1,y1,x2,y2)
                self.capacity_field = LineWithEditableText(tk, canvas, text_x ,text_y-36,self.capacity,"Cap")
                self.minimum_field = LineWithEditableText(tk, canvas, text_x, text_y - 12, self.minimum, "Min")
                # Crear la línea de la arista unidireccional con las coordenadas ajustadas
                self.line = canvas.create_line(x_inicio, y_inicio, x_fin, y_fin, arrow=tk.LAST, fill="black", width=9, arrowshape=(20, 25, 10), tags=("edge",))
                self.text_item = canvas.create_text(text_x +40,text_y-24, text=self.porcentaje,tags=("node",),font=("Helvetica", 12))  # Cambia 12 al tamaño de fuente deseado

            else:
                # Crear la línea de la arista bidireccional con las coordenadas de los nodos
                self.line = canvas.create_line(x_inicio, y_inicio, x_fin, y_fin,arrow=tk.BOTH, fill="black", width=12, arrowshape=(20, 25, 10), tags=("edge",))

    def update_edge(self,canvas):
        x1, y1 = self.start_node.x, self.start_node.y

        if self.type_edge == "entrance":
            xText = (x1 - 125)
            canvas.coords(self.line, x1-125, y1, x1-40, y1)
            self.capacity_field.update_text(canvas, xText+60, y1-24)
            #self.minimum_field.update_text(canvas, xText + 60, y1 - 48)
        elif self.type_edge == "exit":
            xText = (x1 + 125)
            canvas.coords(self.line, x1+40, y1, x1+125, y1)
            self.capacity_field.update_text(canvas, xText+60, y1 - 24)
            canvas.coords(self.text_item, self.start_node.x + 135 + 95, y1 - 24)

        else:
            x_inicio,y_inicio,x_fin,y_fin,distancia=self.calc_coord(x1,y1)

            text_x, text_y, angle=self.calc_angle(x_inicio,y_inicio,x_fin,y_fin)
            self.capacity_field.update_text(canvas, text_x, text_y - 36)
            self.minimum_field.update_text(canvas, text_x, text_y - 12)
            canvas.coords(self.line, x_inicio, y_inicio, x_fin, y_fin)
            canvas.coords(self.text_item, text_x+ 40, text_y - 24)

    def remove(self,canvas):
        canvas.delete(self.line)
        canvas.delete(self.text_item)
        self.capacity_field.delete_text(canvas)
        if self.type_edge == "one_way":
            self.minimum_field.delete_text(canvas)

    def remove_edge_only(self,canvas):
        canvas.delete(self.line)
        canvas.delete(self.text_item)
        self.capacity_field.delete_text(canvas)
        if self.type_edge == "one_way":
            self.minimum_field.delete_text(canvas)

    def calc_angle(self,x_inicio,y_inicio,x_fin,y_fin):
        # Calcular el ángulo de la línea de la arista
        angle = (degrees(atan2(y_fin - y_inicio, x_fin - x_inicio)))*(-1)

        if(angle>=90 and angle<=180):
            angle=angle+180
        elif(angle<=-90 and angle>=-180):
            angle=angle+180

        if((angle<=-50 and angle>=-90) or (angle<=320 and angle>=270)):

            text_x = ((x_inicio + x_fin) / 2)+15
            text_y = (y_inicio + y_fin) / 2
        elif (angle >= 45 and angle <= 90):

            text_x = ((x_inicio + x_fin) / 2) - 15
            text_y = (y_inicio + y_fin) / 2
        else:

            text_x = (x_inicio + x_fin) / 2
            text_y = (y_inicio + y_fin) / 2


        return text_x, text_y, angle
    def calc_coord(self,x1,y1):
        x2, y2 = self.end_node.x, self.end_node.y
        # x1, y1, r1, x2, y2, r2
        dx = x2 - x1
        dy = y2 - y1
        distancia = (dx ** 2 + dy ** 2) ** 0.5
        if distancia == 0:
            return
        x_inicio = x1 + self.start_node.radio * dx / distancia
        y_inicio = y1 + self.start_node.radio * dy / distancia
        x_fin = x2 - self.end_node.radio * dx / distancia
        y_fin = y2 - self.end_node.radio * dy / distancia

        return x_inicio, y_inicio, x_fin, y_fin,distancia

    def set_value(self):
        if self.type_edge == "one_way":
            self.capacity = self.capacity_field.get_text()
            self.minimum = self.minimum_field.get_text()
        elif self.type_edge == "entrance":
            self.minimum=0
            self.entrada=self.capacity_field.get_text()
        elif self.type_edge == "exit":
            self.capacity=self.capacity_field.get_text()
            self.minimum = 0

    def print_info(self):
        #self.set_value()
        if self.start_node != None and self.end_node != None:
            print("Desde: ",self.start_node.name, " Hacia: ",self.end_node.name, " Capacidad: ",self.capacity, " %: ",self.minimum, " Entrada_carro: ",self.entrada," Porcent: ",self.porcentaje," Exit: ",self.salida )

        else:
            if self.type_edge == "entrance":
                print("Desde Only: ", self.start_node.name, " Entrada_carro: ",self.entrada," Name: ",self.name," Type: ",self.type_edge)
            if self.type_edge == "exit":
                print("Desde Only: ", self.start_node.name, " Capacidad: ",self.capacity," Entrada_carro: ",self.entrada," Salida_carro: ",self.salida," Porcent: ",self.porcentaje, " Name: ",self.name," Type: ",self.type_edge)

    def copy_edge(self,edge):
        new_edge = Edge(edge.id,  edge.start_node, edge.end_node,
                        edge.type_edge, edge.capacity, edge.minimum)
        new_edge.line = edge.line
        #new_edge.name=edge.type_edge+f"{self.id}".strip()
        new_edge.text_item = edge.text_item
        new_edge.capacity_field = edge.capacity_field
        new_edge.minimum_field = edge.minimum_field
        new_edge.porcentaje = edge.porcentaje
        new_edge.entrada = edge.entrada
        new_edge.salida = edge.salida
        return new_edge


    def calc_car_exits(self):

        numero = (self.entrada * self.porcentaje)/100
        if numero % 1 > 0.5:
            self.salida += math.ceil(numero)

            if  (self.salida > self.capacity):
                self.salida = self.capacity
                #print("Type: ", self.type_edge, " num: ", self.salida)

        else:
            self.salida += math.floor(numero)
            if (self.salida > self.capacity):
                self.salida = self.capacity
                #print("Type: ",self.type_edge, " num: ",self.salida)

    def reset_value(self):
        if self.type_edge == "one_way":
            self.entrada = 0
            self.salida = 0
        if self.type_edge == "exit":
            self.entrada = 0
            self.salida = 0


