import pickle
from tkinter import filedialog


class InstanceSaver:
    def __init__(self, filename):
        self.filename = filename

    def save_instance(self, instance):
        file_path = filedialog.asksaveasfilename(defaultextension=".pkl",
                                                 filetypes=[("Pickle Files", "*.pkl"), ("All Files", "*.*")])
        # Verificar si se ha seleccionado un archivo
        if file_path:
            # Guardar el modelo en el archivo seleccionado
            with open(file_path, "wb") as file:
                pickle.dump(instance, file)
            # Mostrar un mensaje de éxito
            print("Instance saved successfully")
        return file_path
        #with open(self.filename, 'wb') as f:
        #    pickle.dump(instance, f)'''
    def update_instance(self,file_path, instance):
        with open(file_path, 'wb') as f:
            pickle.dump(instance, f)
        print("Instance updated successfully")

    def load_instance(self):
        # Abrir el file chooser para seleccionar el archivo a cargar
        file_path = filedialog.askopenfilename(filetypes=[("Pickle Files", "*.pkl"), ("All Files", "*.*")])

        # Verificar si se ha seleccionado un archivo
        if file_path:
            # Cargar el modelo desde el archivo seleccionado
            with open(file_path, "rb") as file:
                instance = pickle.load(file)
                print("Instance loaded successfully")
                return instance,file_path

            # Mostrar un mensaje con el contenido del modelo cargado
            #tk.messagebox.showinfo("Cargar Modelo", f"Modelo cargado:\n{loaded_model}")
        #with open(self.filename, 'rb') as f:
        #    instance = pickle.load(f)
        #    return instance'''