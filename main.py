from UI.StreetSystemGUI import StreetSystemGUI
import tkinter as tk

def Main():
    # Crear la ventana principal de la aplicación
    root = tk.Tk()
    app = StreetSystemGUI(root)
    root.mainloop()
if __name__ == "__main__":
    Main()