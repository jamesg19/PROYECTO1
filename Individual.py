import math
import random

from Logica.Grafo import Grafo


class Individual:
    def __init__(self, node,edge):
        self.node = [node.copy_node(node) for node in node]
        self.edge = [edge.copy_edge(edge) for edge in edge]
        self.num_entry_cars = 0
        self.num_exit_cars = 0
        self.num_car_exits=0 #variable que guarda la cantidad de carros que lograron salir del sistema
        self.aptitude=0
        self.genes=[]
        self.get_num_entry_cars()
        self.get_num_exit_cars()

    def random(self):
        #print("--------------------------------------------------------")
        #print("--------------------------------------------------------")
        TOTAL = 100

        cumulative = 0
        grafo=Grafo(self.node,self.edge)
        self.node=grafo.orden_topologico()

        for node in self.node:

            node.calc_num_cars_in_entrance(self.edge)
            edge_in_node=[]
            for edge in self.edge:

                if node.name == edge.start_node.name and (edge.type_edge =="one_way" ):
                    edge_in_node.append(edge)
            for edge in self.edge:
                if node.name == edge.start_node.name and ( edge.type_edge =="exit"):
                    edge_in_node.append(edge)
            #print(f"------------------------ITERA EN NODE {node.name} Aristas: {len(edge_in_node)}-------------------------")
            #print(" Entrada: ", node.entrada)
            node.calc_num_cars_in_node(edge_in_node)
            #print("Con aristas: ",len(edge_in_node))
            edge_in_node = sorted(edge_in_node, key=lambda x: x.capacity, reverse=False)
            #random.shuffle(edge_in_node)
            if (len(edge_in_node) == 1):
                #One Way
                if edge_in_node[0].end_node != None:
                    edge_in_node[0].porcentaje=100
                    edge_in_node[0].entrada = node.entrada
                    edge_in_node[0].calc_car_exits()
                    edge_in_node[0].end_node.entrada += edge_in_node[0].salida
                    #print("Asigna salida a end node: ",edge_in_node[0].end_node.name, " Valor: ",edge_in_node[0].salida)
                    continue
                else:
                    node.calc_num_car_in_node_exit(self.edge,edge_in_node[0])
                    edge_in_node[0].start_node.entrada += edge_in_node[0].salida
                    #print("Asigna salida a start_node: ", edge_in_node[0].start_node.name, " Valor: ",edge_in_node[0].salida)
                    #print(edge_in_node[0].salida)
                #Exit
            # Para sumar todos los valores del atributo minimum
            sum_minimum = sum(edge.minimum for edge in edge_in_node)
            flag_edge_cero = 0

            for edge in edge_in_node:
                #for One way
                if edge.type_edge == "one_way":
                    #print("One way")
                    if edge.capacity == 0 or edge.minimum ==0:
                        sum_minimum -= edge.minimum
                        flag_edge_cero +=1
                        continue

                    sum_porcentaje = sum(edge.porcentaje for edge in edge_in_node)
                    sum_minimum -= edge.minimum

                    if sum_porcentaje == 0 and flag_edge_cero == 0:
                        edge.porcentaje = random.randint(edge.minimum, (TOTAL - sum_minimum ))
                        #print("Porcentaje ", edge.porcentaje)
                    else:
                        #Verifica si es la ultima en la fila
                        if edge.name == edge_in_node[-1].name:

                            if flag_edge_cero > 0:
                                edge.porcentaje = (TOTAL - sum_minimum)
                                #print("Porcentaje ", edge.porcentaje)
                            else:
                                edge.porcentaje = (TOTAL - sum_porcentaje)
                                #print("Porcentaje ", edge.porcentaje)
                        else:
                            edge.porcentaje = random.randint(edge.minimum, (TOTAL - sum_porcentaje - sum_minimum))
                            #print("Porcentaje ", edge.porcentaje)

                    edge.entrada += node.entrada
                    edge.calc_car_exits()
                    #print("Asigna salidaaa: ", edge.salida)
                    #print("En Nodo ",edge.end_node.name)
                    edge.end_node.entrada += edge.salida
                #For exits
                if edge.type_edge == "exit":

                    if edge.capacity == 0 :

                        sum_minimum -= edge.minimum
                        flag_edge_cero +=1
                        continue

                    sum_porcentaje = sum(edge.porcentaje for edge in edge_in_node)
                    sum_minimum -= edge.minimum

                    #Verifica si es la ultima en la fila
                    if edge.name == edge_in_node[-1].name:

                        if flag_edge_cero > 0:
                            edge.porcentaje = (TOTAL - sum_minimum)
                        else:
                            edge.porcentaje = (TOTAL - sum_porcentaje)
                    else:
                        edge.porcentaje = random.randint(edge.minimum, (TOTAL - sum_porcentaje - sum_minimum))

                    edge.entrada += node.entrada
                    edge.calc_car_exits()
                    self.num_car_exits += edge.salida
        #self.info_edges()
        #print("\n\n")

    def calculate_with_new_genes(self):

        grafo = Grafo(self.node, self.edge)
        self.node = grafo.orden_topologico()

        for node in self.node:

            node.calc_num_cars_in_entrance(self.edge)
            edge_in_node = []

            for edge in self.edge:
                if node.name == edge.start_node.name and (edge.type_edge == "one_way"):
                    edge_in_node.append(edge)
            for edge in self.edge:
                if node.name == edge.start_node.name and (edge.type_edge == "exit"):
                    edge_in_node.append(edge)

            node.calc_num_cars_in_node(edge_in_node)

            edge_in_node = sorted(edge_in_node, key=lambda x: x.capacity, reverse=False)

            if (len(edge_in_node) == 1):
                # One Way
                if edge_in_node[0].end_node != None:
                    edge_in_node[0].entrada += node.entrada
                    edge_in_node[0].calc_car_exits()
                    edge_in_node[0].end_node.entrada += edge_in_node[0].salida

                    continue
                else:
                    node.calc_num_car_in_node_exit(self.edge, edge_in_node[0])
                    edge_in_node[0].start_node.entrada = edge_in_node[0].salida

            for edge in edge_in_node:
                # for One way
                if edge.type_edge == "one_way":
                    edge.entrada += node.entrada
                    edge.calc_car_exits()
                    edge.end_node.entrada += edge.salida

                # For exits
                if edge.type_edge == "exit":
                    edge.entrada += node.entrada
                    edge.calc_car_exits()
                    self.num_car_exits += edge.salida
        '''print("Nuevos genes recalculados por mutacion")
        self.info_edges()'''
    def mutation(self,nodeM):
        #print("Mutacion")
        TOTAL = 100
        cumulative = 0

        #for node in self.node:

        #nodeM.calc_num_cars_in_entrance(self.edge)
        edge_in_node = []
        for edge in self.edge:

            if nodeM.name == edge.start_node.name and (edge.type_edge =="one_way" ):
                edge.porcentaje=0
                edge_in_node.append(edge)
        for edge in self.edge:
            if nodeM.name == edge.start_node.name and ( edge.type_edge =="exit"):
                edge.porcentaje = 0
                edge_in_node.append(edge)
        #print(f"------------------------ITERA EN NODE {node.name} Aristas: {len(edge_in_node)}-------------------------")

        #nodeM.calc_num_cars_in_node(edge_in_node)
        #print("Con aristas: ",len(edge_in_node))
        edge_in_node = sorted(edge_in_node, key=lambda x: x.capacity, reverse=False)
        #random.shuffle(edge_in_node)
        if (len(edge_in_node) == 1):
            #One Way
            if edge_in_node[0].end_node != None:
                edge_in_node[0].porcentaje=100
                #edge_in_node[0].entrada = nodeM.entrada
                #edge_in_node[0].calc_car_exits()
                #edge_in_node[0].end_node.entrada = edge_in_node[0].salida
                return
            else:
                nodeM.calc_num_car_in_node_exit(self.edge,edge_in_node[0])
                edge_in_node[0].start_node.entrada = edge_in_node[0].salida

        # Para sumar todos los valores del atributo minimum
        sum_minimum = sum(edge.minimum for edge in edge_in_node)
        flag_edge_cero = 0

        for edge in edge_in_node:
            #for One way
            if edge.type_edge == "one_way":

                if edge.capacity == 0 or edge.minimum ==0:
                    sum_minimum -= edge.minimum
                    flag_edge_cero +=1
                    continue

                sum_porcentaje = sum(edge.porcentaje for edge in edge_in_node)
                sum_minimum -= edge.minimum

                if sum_porcentaje == 0 and flag_edge_cero == 0:
                    edge.porcentaje = random.randint(edge.minimum, (TOTAL - sum_minimum ))
                else:
                    #Verifica si es la ultima en la fila
                    if edge.name == edge_in_node[-1].name:

                        if flag_edge_cero > 0:
                            edge.porcentaje = (TOTAL - sum_minimum)
                        else:
                            edge.porcentaje = (TOTAL - sum_porcentaje)
                    else:
                        edge.porcentaje = random.randint(edge.minimum, (TOTAL - sum_porcentaje - sum_minimum))
                edge.entrada += nodeM.entrada
                edge.calc_car_exits()
                edge.end_node.entrada += edge.salida
            #For exits
            if edge.type_edge == "exit":

                if edge.capacity == 0 :

                    sum_minimum -= edge.minimum
                    flag_edge_cero +=1
                    continue

                sum_porcentaje = sum(edge.porcentaje for edge in edge_in_node)
                sum_minimum -= edge.minimum

                #Verifica si es la ultima en la fila
                if edge.name == edge_in_node[-1].name:

                    if flag_edge_cero > 0:
                        edge.porcentaje = (TOTAL - sum_minimum)
                    else:
                        edge.porcentaje = (TOTAL - sum_porcentaje)
                else:
                    edge.porcentaje = random.randint(edge.minimum, (TOTAL - sum_porcentaje - sum_minimum))

                edge.entrada += nodeM.entrada
                edge.calc_car_exits()
                self.num_car_exits += edge.salida
        #self.info_edges()
        print("\n\n")

    def get_num_entry_cars(self):
        self.num_entry_cars=0
        for edgeItem in self.edge:
            if edgeItem.type_edge=="entrance":
                self.num_entry_cars+=edgeItem.entrada
    def get_num_exit_cars(self):
        self.num_exit_cars=0
        for edgeItem in self.edge:
            if edgeItem.type_edge == "exit":
                self.num_exit_cars += edgeItem.salida


    def info_edges(self):
        print("\n_______Nodes______")
        for item in self.node:
            item.print_info()
        print("\n_______Edges______")
        for item in self.edge:
            item.print_info()
            # item.print_info()

    def get_aptitude(self):
        if self.num_entry_cars == 0:
            return 0
        self.aptitude = math.floor((self.num_car_exits/self.num_entry_cars)*100)


