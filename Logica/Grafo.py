from collections import defaultdict
class Grafo:
    def __init__(self,nodes,edges):
        self.nodes = set()
        self.NodeG=nodes
        self.EdgeG = edges
        self.edges = defaultdict(list)
        self.grado_entrada = defaultdict(int)
        self.add_node_t()
        self.add_edge_t()

    def add_node_t(self):
        for e in self.EdgeG:
            for i in self.NodeG:
                if e.end_node != None:
                    if i.name == e.start_node.name:
                        e.start_node = i
                    if i.name == e.end_node.name:
                        e.end_node = i


                self.nodes.add(i)

    def add_edge_t(self):
        for i in self.EdgeG:
            self.edges[i.start_node].append(i)
            self.grado_entrada[i.end_node] += 1

    def orden_topologico(self):
        nodos_sin_entrada = [nodo for nodo in self.nodes if self.grado_entrada[nodo] == 0]
        orden_topologico = []

        while nodos_sin_entrada:
            nodo_actual = nodos_sin_entrada.pop(0)

            orden_topologico.append(nodo_actual)

            for arista in self.edges[nodo_actual]:
                self.grado_entrada[arista.end_node] -= 1
                if self.grado_entrada[arista.end_node] == 0:
                    nodos_sin_entrada.append(arista.end_node)

        if len(orden_topologico) != len(self.nodes):
            ''' print("El grafo contiene un ciclo")
            print("Topologia: ",len(orden_topologico))
            print("Nodos: ", len(self.nodes))'''
            return self.NodeG
            #raise ValueError("El grafo contiene un ciclo")
        #print("Orden Topologico")

        return orden_topologico

