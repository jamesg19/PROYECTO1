import copy
from UI.Edge import Edge
from UI.Node import Node


class Model:
    def __init__(self, nodess,edgess,node_counter,edge_counter):
        self.nodes1=nodess
        self.edges1=edgess
        # Contador para asignar nombres a los nodos
        self.node_counter = node_counter
        # Contador para asignar nombres a los nodos
        self.edge_counter = edge_counter


    def prepare_object_to_save(self):

        for edgeItem in self.edges1:
            if edgeItem.capacity_field is not None:
                edgeItem.capacity = edgeItem.capacity_field.get_text()
            if edgeItem.minimum_field is not None:
                edgeItem.minimum = edgeItem.minimum_field.get_text()

        # Clonar la lista de objetos Edge
        lista_edges_clonada = [self.clone_edge(edge) for edge in self.edges1]

        for edgeItem in lista_edges_clonada:

            if edgeItem.capacity_field is not None:
                edgeItem.capacity_field.delete_entry()

            if edgeItem.minimum_field is not None:
                edgeItem.minimum_field.delete_entry()

        # Clonar la lista de objetos Node
        lista_nodes_clonada = [self.clone_node(node) for node in self.nodes1]

        self.edges1= lista_edges_clonada
        self.nodes1 = lista_nodes_clonada

        for edg in self.edges1:
            for nod in self.nodes1:

                if edg.start_node.name==nod.name:
                    edg.start_node=nod

                if (edg.end_node != None) and (edg.end_node.name==nod.name):
                    edg.end_node = nod

    def clone_edge(self,edge):
        return Edge(edge.id,  edge.start_node, edge.end_node, edge.type_edge, edge.capacity, edge.minimum)

    def clone_node(self,node):
        return Node(None, node.x, node.y, node.name, node.node_type, node.color)


    # Getter para el atributo nodes
    @property
    def nodes1(self):
        return self._nodes1

    @nodes1.setter
    def nodes1(self, value):
        self._nodes1 = value

    # Getter para el atributo edges
    @property
    def edges1(self):
        return self._edges1

    @edges1.setter
    def edges1(self, value):
        self._edges1 = value

